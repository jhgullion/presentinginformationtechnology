<?php
session_start();
//if ($_SESSION['validUser'] == "yes")	//If this is a valid user allow access to this page
//{

	try {
	  
	  require 'database/connectPDO.php';	//CONNECT to the database
	  
	  //mysql DATE stores data in a YYYY-MM-DD format
	  $todaysDate = date("Y-m-d");		//use today's date as the default input to the date( )
	  
	  //Create the SQL command string
	  $sql = "SELECT ";
	  $sql .= "presenters_fName, ";
	  $sql .= "presenters_lName, ";
	  $sql .= "presenters_city, ";
	  $sql .= "presenters_st, ";
	  $sql .= "presenters_zip, ";
	  $sql .= "presenters_email, ";
	  $sql .= "presenters_dateAdded "; //Last column does NOT have a comma after it.
	  $sql .= "FROM pit_presenters";
	  
	  //PREPARE the SQL statement
	  $stmt = $conn->prepare($sql);
	  
	  //EXECUTE the prepared statement
	  $stmt->execute();		
	  
	  //RESULT object contains an associative array
	  $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
  }
  
  catch(PDOException $e)
  {
	  $message = "There has been a problem. The system administrator has been contacted. Please try again later.";

	  error_log($e->getMessage());			//Delivers a developer defined error message to the PHP log file at c:\xampp/php\logs\php_error_log
	  error_log($e->getLine());
	  error_log(var_dump(debug_backtrace()));
  
	  //Clean up any variables or connections that have been left hanging by this error.		
  
	  header('Location: files/505_error_response_page.php');	//sends control to a User friendly page					
  }

?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Presenting Information Technology</title>

	<link rel="stylesheet" href="css/pit.css">

</head>

<body>

<div id="container">

	<header>
    	<h1>Presenting Information Technology</h1>
    </header>
    
	<?php require 'includes/navigation.php' ?>
    
    <main>
    
        <h1>Display Registered Presenters</h1>
        
        <?php 
			while( $row=$stmt->fetch(PDO::FETCH_ASSOC)) {
		?>		
				<div class="presenterBlock">
					<div>
						<span class="displayfName"><?php echo $row['presenters_fName']; ?></span><span class="displaylName"><?php echo " " . $row['presenters_lName']; ?></span>
					</div>
					<div>
						<span class="displayAddress"></span><?php echo $row['presenters_city'] . " " . $row['presenters_st'] . ", " . $row['presenters_zip'] ?>
					</div>
					<div>
						<span class="displayEmail"><?php echo $row['presenters_email'] ?></span>
					</div>
				</div>
        <?php
			}
		?>	
  	
        
	</main>
    
	<footer>
    	<p>Copyright &copy; <script> var d = new Date(); document.write (d.getFullYear());</script> All Rights Reserved</p>
    
    </footer>




</div>
</body>
</html>
